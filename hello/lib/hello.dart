library hello;

import 'package:flutter/material.dart';

/// A Calculator.
class TextWidget extends StatefulWidget {
  final String name;

  TextWidget({Key key, this.name}) : super(key: key);

  _TextWidgetState createState() => _TextWidgetState();
}

class _TextWidgetState extends State<TextWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(widget.name),
    );
  }
}
